module edu.ntnu.oflarsen.idatt2001 {
    requires javafx.controls;
    requires javafx.fxml;


    opens edu.ntnu.oflarsen.idatt2001 to javafx.fxml,java.compiler;
    exports edu.ntnu.oflarsen.idatt2001;
    exports edu.ntnu.oflarsen.idatt2001.controllers;
    opens edu.ntnu.oflarsen.idatt2001.controllers to java.compiler, javafx.fxml;
    exports edu.ntnu.oflarsen.idatt2001.data;
    opens edu.ntnu.oflarsen.idatt2001.data to java.compiler, javafx.fxml;
    exports edu.ntnu.oflarsen.idatt2001.util;
    opens edu.ntnu.oflarsen.idatt2001.util to java.compiler, javafx.fxml;
    exports edu.ntnu.oflarsen.idatt2001.controllers.components;
    opens edu.ntnu.oflarsen.idatt2001.controllers.components to java.compiler, javafx.fxml;
}