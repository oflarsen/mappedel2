package edu.ntnu.oflarsen.idatt2001.pattern.factory;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * This is the Factory class for the GUI
 * The factory can be called from the Client where the designated Node is being returned.
 * The class is used as the factory, where the class Node is the Interface for the different GUI elements
 * The task said that we only had to use components that heritages from Node, and if you were to use FXML,
 * you only had to create the factory class, and not use it.
 */
public abstract class Factory {

    /**
     * Creates an instance of Factory
     */
    public Factory() {
    }

    /**
     * Takes in a Node component name as string and returns the component
     * I only implemented Nodes that I used in the fxml since this makes the most sense
     * I would only have used these Nodes to create the Views in this project
     * @param guiPart
     * @return
     */
    public static Node getGUINode(String guiPart){

        if(guiPart == null){
            return null;
        }
        if(guiPart.equalsIgnoreCase("GRIDPANE")){
            return new GridPane();
        }
        if(guiPart.equalsIgnoreCase("MENUBAR")){
            return new MenuBar();
        }
        if(guiPart.equalsIgnoreCase("HBOX")){
            return new HBox();
        }
        if(guiPart.equalsIgnoreCase("PANE")){
            return new Pane();
        }
        if(guiPart.equalsIgnoreCase("IMAGEVIEW")){
            return new ImageView();
        }
        if(guiPart.equalsIgnoreCase("TEXTFIELD")){
            return new TextField();
        }
        if(guiPart.equalsIgnoreCase("TABLEVIEW")){
            return new TableView<>();
        }
        if(guiPart.equalsIgnoreCase("IMAGELINE")){

        }

        if(guiPart.equalsIgnoreCase("LABEL")){
            return new Label();
        }
        if(guiPart.equalsIgnoreCase("BUTTONBAR")){
            return new Button();
        }
        if(guiPart.equalsIgnoreCase("BUTTON")){
            return new Button();
        }
        if(guiPart.equalsIgnoreCase("GRIDPANE")){
            return new GridPane();
        }

        return null;
    }
}
