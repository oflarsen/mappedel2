package edu.ntnu.oflarsen.idatt2001.data;

import edu.ntnu.oflarsen.idatt2001.data.Patient;

import java.util.*;

/**
 * PatientRegister class. Is a register that keeps track of patients, and where you can add/edit/delete patients
 * as well as some other features like sorting the patients by first name
 * The class uses composition with Patient to create new patients. This makes the most sense, since you can't be a
 * Patient in a hospital, without being registered first.
 */
public class PatientRegister {

    private ArrayList<Patient> patients;

    /**
     * Creates an instance of PatientRegister and a new ArrayList
     */
    public PatientRegister() {
        patients = new ArrayList<>();
    }

    /**
     * Method to add a new patient to the register. Returns the patient created
     * Checks if the patient is already created, if already created, throws an IllegalArgumentException with
     * suiting message
     * @param firstname
     * @param lastname
     * @param diagnosis
     * @param generalPrac
     * @param SSN
     * @return
     */
    public Patient addPatient(String firstname, String lastname, String diagnosis, String generalPrac, String SSN){
        Patient patient = new Patient(firstname,lastname,diagnosis,generalPrac,SSN);
        if(patientAlreadyExists(patient)){
            throw new IllegalArgumentException("Patient already exists in the register");
        }
        patients.add(0, patient);
        return patient;
    }

    /**
     * Method that takes in a PatientRegister and copies that Register. It is important to note that
     * the copy is a deep copy as it should be in composition, ans therefore this is not a problem
     * to do. The data is only copies of real data. Look at private method copyOfArrayList and getPatients for deep copy
     * proof.
     * @param patients
     */
    public void newListOfPatients(PatientRegister patients){
        this.patients = patients.getPatients();
    }

    /**
     * Method to make sure patient already exists in projects
     * @param patient
     * @return
     */
    private boolean patientAlreadyExists(Patient patient){
        return patients.contains(patient);
    }

    /**
     * Method to remove patient from register. Checks first if the patient is already in the register, if not
     * an exception is thrown. If this passes, the method removes the person it finds
     * @param sSN
     */
    public void removePatient(String sSN){

        if(patients.stream().noneMatch(patient -> patient.getSSN().equals(sSN))){
            throw new IllegalArgumentException("Patient selected not in register");
        }
        patients.remove(patients.stream().filter(patient -> patient.getSSN().equals(sSN)).findFirst().get());

    }

    /**
     * Method to edit already existing patient. Checks if the given patient is present in the register, if not an exception
     * is thrown. If he is present new values are set. The SSN can't be changed, but is used to find the patient to edit
     * All the methods where the patients new values are being set, have input checks that are in the Patient class
     * @param firstname
     * @param lastname
     * @param diagnosis
     * @param gP
     * @param sSN
     */
    public void editPatient(String firstname, String lastname,String diagnosis, String gP, String sSN){
        if(!sSN.matches("[0-9]+")){
            throw new IllegalArgumentException("SNN must only be numbers");
        }
        if(sSN.length() != 11){
            throw new IllegalArgumentException("SNN must be 11 digits");
        }
        if(patients.stream().noneMatch(patient -> patient.getSSN().equals(sSN))){
            throw new IllegalArgumentException("Patient selected not in register");
        }
        Patient localPatient = patients.stream().filter(patient -> patient.getSSN().equals(sSN)).findFirst().get();
        localPatient.setFirstName(firstname);
        localPatient.setLastName(lastname);
        localPatient.setGeneralPractitioner(gP);
        localPatient.setDiagnosis(diagnosis);
    }

    /**
     * Private method that returns a deep copy of given ArrayList with patients. This is to fulfill composition, where
     * the user never should get the real objects that are saved, only deep copies of them
     * @param patients
     * @return
     */
    private ArrayList<Patient> copyOfArrayList(ArrayList<Patient> patients){
        ArrayList<Patient> copy = new ArrayList<>();
        patients.forEach(p -> copy.add(new Patient(p.getFirstName(),p.getLastName(),p.getDiagnosis(),p.getGeneralPractitioner(),p.getSSN())));
        return copy;
    }

    /**
     * Returns deep copy of patients in register
     * @return
     */
    public ArrayList<Patient> getPatients() {
        return copyOfArrayList(patients);
    }

    /**
     * Method to sort the list of patients based on first name
     */
    public void sortAlphabetically(){
        patients.sort(
                Comparator.comparing(Patient::getFirstName)
        );
    }

}

