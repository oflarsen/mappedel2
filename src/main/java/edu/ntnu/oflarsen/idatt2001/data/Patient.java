package edu.ntnu.oflarsen.idatt2001.data;

import java.util.Objects;

/**
 * Patient class
 * Each patient has a first name, last name and a immutable Social Security Number
 * I chose to make the SSN final, because the task didn't specify that it should be editable, and this made the most
 * sense.You can change names, diagnosis, and GP, but not SSN.
 * General Practitioner and diagnosis is not a requirement to create a Patient
 */
public class Patient {

    private String firstName;
    private String lastName;
    private final String SSN;
    private String diagnosis;
    private String generalPractitioner;

    /**
     * Creates an instance of Patient. A valid patient is a patient where first and last name are not null, empty or blank
     * If this is the case exceptions get thrown with a fitting message
     * If the SSN that is given is not 11 digits, there is also an exception thrown
     * Other than that it is just normal checks if inputs are null, and trimming inputs from the user
     * @param firstName
     * @param lastName
     * @param diagnosis
     * @param generalPractitioner
     * @param SSN
     */
    public Patient(String firstName, String lastName, String diagnosis, String generalPractitioner, String SSN){
        if(firstName == null){
            throw new IllegalArgumentException("First name can't be null");
        }
        if(firstName.isEmpty() || firstName.isBlank()){
            throw new IllegalArgumentException("First name can't be an empty string");
        }

        this.firstName = firstName.trim();

        if(lastName == null){
            throw new IllegalArgumentException("Last name can't be null");
        }
        if(lastName.isEmpty() || lastName.isBlank()){
            throw new IllegalArgumentException("Last name can't be an empty string");
        }
        this.lastName = lastName.trim();

        if(diagnosis == null){
            this.diagnosis = "";
        }else {
            this.diagnosis = diagnosis.trim();
        }

        if(generalPractitioner == null) {
            this.generalPractitioner = "";
        }else {
            this.generalPractitioner = generalPractitioner.trim();
        }

        if(SSN == null){
            throw new IllegalArgumentException("SNN can't be null");
        }
        if(SSN.isBlank() || SSN.isEmpty()){
            throw new IllegalArgumentException("SNN can't be empty");
        }
        if(!SSN.matches("[0-9]+")){
            throw new IllegalArgumentException("SNN must only be numbers");
        }
        if(SSN.length() != 11){
            throw new IllegalArgumentException("SNN must be 11 digits");
        }

        this.SSN = SSN.trim();
    }


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSSN() {
        return SSN;
    }

    /**
     * Here the same requirements for the first name is set, as in the constructor
     * @param firstName
     */
    public void setFirstName(String firstName) {
        if(firstName == null){
            throw new IllegalArgumentException("First name can't be null");
        }
        if(firstName.isEmpty() || firstName.isBlank()){
            throw new IllegalArgumentException("First name can't be an empty string");
        }
        this.firstName = firstName.trim();
    }
    /**
     * Here the same requirements for the last name is set, as in the constructor
     * @param lastName
     */
    public void setLastName(String lastName) {
        if(lastName == null){
            throw new IllegalArgumentException("Last name can't be null");
        }
        if(lastName.isEmpty() || lastName.isBlank()){
            throw new IllegalArgumentException("Last name can't be an empty string");
        }
        this.lastName = lastName.trim();
    }

    /**
     * Checks if diagnosis is null, if so it is set to "", and if not it is trimmed. This is the same
     * as in the constructor
     * @param diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        if(diagnosis == null){
            this.diagnosis = "";
        }else {
            this.diagnosis = diagnosis.trim();
        }
    }
    /**
     * Checks if gP is null, if so it is set to "", and if not it is trimmed. This is the same
     * as in the constructor
     * @param generalPractitioner
     */
    public void setGeneralPractitioner(String generalPractitioner) {
        if(generalPractitioner == null){
            this.generalPractitioner = "";
        }else{
        this.generalPractitioner = generalPractitioner.trim();
    }}

    public String getDiagnosis() {
        return diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    /**
     * Equals method where a Patient is compared based on SSN. This is natural to do, since no one has the same SSN
     * and this is a immutable String for the patient
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Patient)) return false;
        Patient patient = (Patient) o;
        return SSN.equals(patient.SSN);
    }


    /**
     * ToString method
     * @return
     */
    @Override
    public String toString() {
        return "Patient{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", SSN=" + SSN +
                '}';
    }
}
