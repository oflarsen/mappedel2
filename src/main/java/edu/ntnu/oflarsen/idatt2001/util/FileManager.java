package edu.ntnu.oflarsen.idatt2001.util;

import edu.ntnu.oflarsen.idatt2001.App;
import edu.ntnu.oflarsen.idatt2001.controllers.components.StatusBar;
import edu.ntnu.oflarsen.idatt2001.data.Patient;
import edu.ntnu.oflarsen.idatt2001.data.PatientRegister;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

/**
 * FileManger class
 * This class is the class responsible for saving and reading csv files
 */
public class FileManager {
    private static String row;

    private static String firstName = "";
    private static String lastName = "";
    private static String diagnosis = "";
    private static String gP = "";
    private static String sSN = "";
    private static int fNIndex;
    private static int lNIndex;
    private static int dIndex;
    private static int gPIndex;
    private static int sSNIndex;

    /**
     * Method responsible for reading a csv file with information about patients
     * The method returns a Patient Register
     * @param pathToCSV
     * @return
     * @throws IOException
     */
    public static PatientRegister readCSV(String pathToCSV) throws IOException {
        PatientRegister patientRegister = new PatientRegister();
        //Check for if file exists
        if (!fileExists(pathToCSV)){
            throw new IOException("File not found");
        }
        //Reads file
        BufferedReader csvReader = new BufferedReader(new InputStreamReader(new FileInputStream(pathToCSV), StandardCharsets.UTF_8));

        boolean readFirstLine = false;

        //Makes sure the filetype is valid
        if(!Objects.requireNonNull(pathToCSV).contains(".csv")){
            throw new IllegalArgumentException("The chosen file type is not valid");
        }
        //Had to put in this try to make sure that the tests work due to NullPointerException in StatusBar
        try {
            StatusBar.setStatusBar("Import successful");
        }catch (NullPointerException ignored){

        }
        //Reads every row until it is null
        while ((row = csvReader.readLine()) != null) {
            String[] dataArray = row.split(";", 5);

            ArrayList<String> data = new ArrayList<>(dataArray.length);
            data.addAll(Arrays.asList(dataArray));
            //Checks if the first line is read. If not, this information should not be stored
            if (readFirstLine) {
                try{
                    //Checks the index of different values. See sortCSVTable method
                    if (fNIndex != -1) {
                        firstName = data.get(fNIndex);
                    }
                    if (lNIndex != -1) {
                        lastName = data.get(lNIndex);
                    }
                    if (dIndex != -1) {
                        diagnosis = data.get(dIndex);
                    }
                    if (gPIndex != -1) {
                        gP = data.get(gPIndex);
                    }
                    if (sSNIndex != -1) {
                        sSN = data.get(sSNIndex);
                    }

                    patientRegister.addPatient(firstName, lastName, diagnosis, gP, sSN);
                    firstName = "";
                    lastName = "";
                    diagnosis = "";
                    gP = "";
                    sSN = "";
                }catch (IllegalArgumentException e){
                    //Had to put in this try to make sure that the tests work due to NullPointerException in StatusBar
                    try {
                        StatusBar.setStatusBar("Some of the imports failed: " + e.getMessage());
                    }catch (NullPointerException ignored){

                    }
                }
            } else {
                readFirstLine = true;
                sortCsvTable(data);
            }
        }
        csvReader.close();
        return patientRegister;
    }

    /**
     * Method to make sure the path given has a file that exists
     * @param pathToCSV
     * @return
     */
    private static boolean fileExists(String pathToCSV){
        File csv = new  File(pathToCSV);
        return csv.exists() && csv.isFile() && csv.canRead();

    }

    /**
     * SaveToCSV method. This method saves the current patient register to csv in given path.
     * If the path doesn't contain .csv, this will be added
     * The csv file will be saved as
     * FirstName;LastName;GeneralPractitioner;SSN;Diagnosis
     * @param pathToCsv
     * @param patientRegister
     * @throws IOException
     */
    public static void saveToCSV(String pathToCsv, PatientRegister patientRegister) throws IOException {
        String plusCSV = ".csv";
        if(pathToCsv.contains(".csv")){
            plusCSV = "";
        }

        FileWriter csvWriter = new FileWriter(pathToCsv + plusCSV, StandardCharsets.UTF_8);
        csvWriter.append("FirstName;");
        csvWriter.append("LastName;");
        csvWriter.append("GeneralPractitioner;");
        csvWriter.append("SSN;");
        csvWriter.append("Diagnosis");

        for(Patient p : patientRegister.getPatients()){
            csvWriter.append("\n");
            csvWriter.append(p.getFirstName()).append(";").append(p.getLastName()).append(";").append(p.getGeneralPractitioner()).append(";").append(p.getSSN()).append(";").append(p.getDiagnosis());
        }
        csvWriter.flush();
        csvWriter.close();
    }

    /**
     * Method to sort which index the different inputs of the patient has
     * If there is a missing index, like diagnosis doesn't exist, this won't cause any problems since the index will be -1
     * This is later checked in the readCSV method. Here each value is set to given index to make sure they are correctly imported
     * If the index is -1, the string will just be empty
     * This method is not case sensitive and checks for different types of firstName, like "firstname" and "first name"
     * @param data
     */
    private static void sortCsvTable(ArrayList<String> data){
        data = (ArrayList<String>) data.stream().map(String::toLowerCase).collect(Collectors.toList());

        fNIndex = data.indexOf("firstname");
        if(fNIndex == -1){
            fNIndex = data.indexOf("first name");
        }
        lNIndex = data.indexOf("lastname");
        if(lNIndex == -1){
            lNIndex = data.indexOf("last name");
        }
        dIndex = data.indexOf("diagnosis");

        gPIndex = data.indexOf("generalpractitioner");
        if(gPIndex == -1){
            gPIndex = data.indexOf("general practitioner");
        }
        sSNIndex = data.indexOf("ssn");
        if(sSNIndex == -1){
            sSNIndex = data.indexOf("social security number");
        }if(sSNIndex == -1){
            sSNIndex = data.indexOf("socialsecuritynumber");
        }


    }

}

