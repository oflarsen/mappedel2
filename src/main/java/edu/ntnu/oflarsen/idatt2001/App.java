package edu.ntnu.oflarsen.idatt2001;

import edu.ntnu.oflarsen.idatt2001.controllers.Controller;
import edu.ntnu.oflarsen.idatt2001.data.PatientRegister;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * JavaFX App
 * The main application. Opens the PatientRegister View
 */
public class App extends Application {
    @FXML
    private static Stage dialog;
    private static Scene dialogScene;
    private static Scene scene;
    private static Stage stage;
    private static PatientRegister patientRegister;
    @FXML
    private static FXMLLoader fxmlLoader;

    /**
     * Method to start the javaFX GUI program
     * Here the main scene is being loaded and showed
     * @param stage
     * @throws IOException
     */
    @Override
    public void start(Stage stage) throws IOException {
        App.stage = stage;
        //Creates the systems patient register
        patientRegister = new PatientRegister();
        scene = new Scene(loadFXML("PatientRegister"), 817, 380);
        stage.setScene(scene);
        stage.setTitle("Patient Register v0.1-SNAPSHOT");
        stage.show();
    }

    /**
     * Initializes a custom dialog. This is used to open add/edit patient view
     * This returns a controller so that the method can be used if expanding the project
     * @param fxml
     * @param title
     * @return
     * @throws IOException
     */
    public static Controller initializeCustomDialog(String fxml, String title) throws IOException{
        dialog = new Stage();
        dialogScene = new Scene(loadFXML(fxml), 576,402);
        dialog.setScene(dialogScene);
        dialog.setTitle(title);
        return getFxml().getController();
    }

    /**
     * Returns the open dialog / last set dialog
     * @return
     */
    public static Stage getDialog(){
        return dialog;
    }

    /**
     * Closes the open dialog
     */
    public static void closeDialog(){
        dialog.close();
    }

    /**
     * Loads the FXML
     * @param fxml
     * @return
     * @throws IOException
     */
    private static Parent loadFXML(String fxml) throws IOException {
        fxmlLoader = new FXMLLoader(App.class.getResource("/edu/ntnu/oflarsen/idatt2001/fxml/" + fxml + ".fxml"));
        return fxmlLoader.load();
    }

    /**
     * Returns FXMLLoader
     * @return
     */
    private static FXMLLoader getFxml(){
        return fxmlLoader;
    }

    /**
     * Returns primary scene
     * @return
     */
    public static Scene getPrimaryScene(){
        return scene;
    }

    /**
     * Returns set stage
     * @return
     */
    public static Stage getStage(){
        return stage;
    }

    public static void main(String[] args) {
        launch();
    }

    /**
     * Returns the patientRegister created in App
     * @return
     */
    public static PatientRegister getPatientRegister(){
        return patientRegister;
    }

}