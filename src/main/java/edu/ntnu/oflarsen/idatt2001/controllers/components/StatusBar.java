package edu.ntnu.oflarsen.idatt2001.controllers.components;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;

/**
 * Class StatusBar
 * The thought behind this class is to be able to change the status bar in the fxml for PatientRegister not only in the
 * PatientRegisterController. This makes it possible to make a more flexible status bar
 */
public class StatusBar {
    @FXML
    private static TextField status;

    /**
     * Creates an instance of statusBar. This method is only used in initialize to PatientRegisterController to avoid
     * NullPointerExceptions since the TextField status is not set
     * @param status
     */
    public StatusBar(TextField status){
        StatusBar.status = status;
    }

    /**
     * Static method where the string that gets taken in is being set as status
     * @param status
     */
    public static void setStatusBar(String status){
        StatusBar.status.setText("Status: " + status);
    }
}
