package edu.ntnu.oflarsen.idatt2001.controllers;

import edu.ntnu.oflarsen.idatt2001.App;
import edu.ntnu.oflarsen.idatt2001.util.FileManager;
import edu.ntnu.oflarsen.idatt2001.data.Patient;
import edu.ntnu.oflarsen.idatt2001.controllers.components.StatusBar;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import javafx.stage.Modality;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Controller for PatientRegister fxml
 * Extends the class Controller
 * This Controller is the controller for the view of the main stage, PatientRegister
 */
public class PatientRegisterController extends Controller {

    private Scene scene;

    @FXML
    private MenuItem menuIAddP;
    @FXML
    private MenuItem menuIEditP;
    @FXML
    private MenuItem menuIDeleteP;
    @FXML
    private TableColumn<Patient, String> tblFirstName;
    @FXML
    private TableColumn<Patient, String> tblLastName;
    @FXML
    private TableColumn<Patient, String> tblSSN;
    @FXML
    private TableView<Patient> tblView;
    @FXML
    private ImageView imgEditPatient;
    @FXML
    private ImageView imgAddPatient;
    @FXML
    private ImageView imgRemovePatient;
    @FXML
    private MenuItem menuIAbout;
    @FXML
    public TextField status;
    @FXML
    private MenuItem menuIFromCsv;
    @FXML
    private MenuItem menuIToCsv;
    @FXML
    private TableColumn<Patient, String> tblGP;
    @FXML
    private MenuItem menuIExit;
    @FXML
    private TableColumn<Patient, String> tblDiagnosis;

    /**
     * Initializes the stage.
     * Initializes the statusBar for the StatusBar class
     * Imports the given patients.csv class when the program starts, fills the table with the register and sorts the register
     * by first name
     * @throws IOException
     */
    @FXML
    private void initialize() throws IOException {

        StatusBar statusBarInit = new StatusBar(status);
        StatusBar.setStatusBar("OK");
        //Test data
        App.getPatientRegister().newListOfPatients(FileManager.readCSV("src/main/resources/edu/ntnu/oflarsen/idatt2001/csv/Patients.csv"));

        App.getPatientRegister().sortAlphabetically();
        fillTable(App.getPatientRegister().getPatients());
        scene = App.getPrimaryScene();
    }
    /**
     * Keyshortcuts for the PatientRegister view
     * A = addPatient
     * E = editPatient
     * DELETE = deletePatient
     */
    @FXML
    private void onKeyHandler(KeyEvent keyEvent) throws IOException {
        if(keyEvent.getCode() == KeyCode.A){
            addPatient();
        }else if(keyEvent.getCode() == KeyCode.E){
            editPatient();
        }else if(keyEvent.getCode() == KeyCode.DELETE){
            removeSelectedPatient();
        }
        update();
    }

    /**
     * EventHandler for the Menu. Each MenuItem does something when clicked
     * add, edit, remove patient
     * About box
     * Load, Save file
     * Exit that gives the user a reminder that if they close the application won't save unsaved changes
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    private void actionEventHandler(ActionEvent actionEvent) throws IOException {
        //Can't use switch because getSource() is object
        if(actionEvent.getSource() == menuIAddP){
            addPatient();
        }else if(actionEvent.getSource() == menuIEditP){
            editPatient();
        }else if(actionEvent.getSource() == menuIDeleteP){
            removeSelectedPatient();
        }else if(actionEvent.getSource() == menuIAbout){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("About");
            alert.setHeaderText("Patient Register\nv0.1-SNAPSHOT");
            alert.setContentText("Made by Olav Larsen\n2021-05-05");

            alert.showAndWait();
        }else if(actionEvent.getSource() == menuIFromCsv){
            fileChooserLoadFile();
        }else if(actionEvent.getSource() == menuIToCsv){
            fileChooserSaveFile();
        }else if(actionEvent.getSource() == menuIExit){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to exit the application?\nInformation that is not saved will be lost!", ButtonType.YES, ButtonType.CANCEL);
            alert.showAndWait();
            if (alert.getResult() == ButtonType.YES) {
                App.getStage().close();
            }
        }
        update();
    }

    /**
     * Method for handling clicking on the different pictures.
     * @param mouseEvent
     * @throws IOException
     */
    @FXML
    private void onMouseClickEventHandler(MouseEvent mouseEvent) throws IOException {
        if(mouseEvent.getSource() == imgAddPatient){
            addPatient();
        }else if(mouseEvent.getSource() == imgEditPatient){
            editPatient();
        }else if(mouseEvent.getSource() == imgRemovePatient){
            removeSelectedPatient();
        }
        update();
    }

    /**
     * Fills the table with the given arrayList of patients
     * @param patients
     */
    private void fillTable(ArrayList<Patient> patients){
        tblFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        tblLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        tblSSN.setCellValueFactory(new PropertyValueFactory<>("SSN"));
        tblGP.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));
        tblDiagnosis.setCellValueFactory(new PropertyValueFactory<>("diagnosis"));
        patients.forEach(p -> tblView.getItems().add(new Patient( p.getFirstName(),p.getLastName(),p.getDiagnosis(),p.getGeneralPractitioner(),p.getSSN())));
    }

    /**
     * Private method that clears the table
     */
    private void clearTable(){
        tblView.getItems().clear();
    }

    /**
     * Returns the selected patient in the table
     * @return
     */
    public Patient getSelectedPatient(){
        return tblView.getSelectionModel().getSelectedItem();
    }

    /**
     * Method to open addPatientDetails dialog box
     * Not able to interact PatientRegisterView when this box is open
     * @throws IOException
     */
    private void addPatient() throws IOException {

        App.initializeCustomDialog("PatientDetails", "Patient Details - Add");
        App.getDialog().initModality(Modality.APPLICATION_MODAL);
        App.getDialog().showAndWait();
    }
    /**
     * Method to open editPatientDetails dialog box
     * Not able to interact with PatientRegisterView when this box is open
     * @throws IOException
     */
    private void editPatient() throws IOException {

        PatientDetailsController patientDetailsController = (PatientDetailsController) App.initializeCustomDialog("PatientDetails", "Patient Details - Edit");
        if(getSelectedPatient() == null){
            StatusBar.setStatusBar("No patient is selected");
        }else {
            patientDetailsController.fillPatient(getSelectedPatient());
            App.getDialog().initModality(Modality.APPLICATION_MODAL);
            App.getDialog().showAndWait();
        }
    }

    /**
     * Method to remove selected patient in table from register
     * Gives the client a confirmation box to make sure they want to delete the patient
     * Updates the statusbar based on outcome
     */
    private void removeSelectedPatient(){
        try {
            if(getSelectedPatient() == null){
                throw new NullPointerException("Failed: No patient is selected");
            }
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to delete this item?", ButtonType.YES, ButtonType.CANCEL);
            alert.showAndWait();

            if (alert.getResult() == ButtonType.YES) {
                try{
                    App.getPatientRegister().removePatient(getSelectedPatient().getSSN());
                    StatusBar.setStatusBar("Patient removed successfully");
                }catch (IllegalArgumentException e){
                    StatusBar.setStatusBar("Remove failed");
                }
            }
        }catch (NullPointerException e){
            StatusBar.setStatusBar("Failed: No patient is selected");
        }
        update();
    }

    /**
     * Method to update the view
     * The table is being cleared, and the register is loaded in again
     */
    public void update() {
        clearTable();
        fillTable(App.getPatientRegister().getPatients());
    }

    /**
     * FileChooserLoadFile method. This method opens the fileChooser where the client can click on designated file to load
     * If this is an csv file the patients will be loaded into the register
     * There are tests to make sure the file is csv, null etc
     */
    private void fileChooserLoadFile(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load file");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("All Files", "*.*"));
        //Gets the destination of where to load file from
        File destination = fileChooser.showOpenDialog(App.getStage());
        try {

            try {
                //Reads file and adds as new register
                if(!destination.isFile() || !destination.canRead() || !destination.exists()){
                    throw new IllegalArgumentException("The chosen file type is not valid");
                }
                App.getPatientRegister().newListOfPatients(FileManager.readCSV(destination.toPath().toString()));
                App.getPatientRegister().sortAlphabetically();
            } catch (IOException e) {
                StatusBar.setStatusBar("Error: " + e.getMessage());
            }catch (IllegalArgumentException e){
                if(e.getMessage().equals("The chosen file type is not valid")){
                    StatusBar.setStatusBar("The chosen file type is not valid");
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setContentText("The chosen file type is not valid.\n" +
                            "Please choose another file or cancel the operation.");
                    alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
                    alert.showAndWait();
                    fileChooserLoadFile();
                }else{
                    StatusBar.setStatusBar("Error: file to read is an invalid file");
                }
            }
        }catch (NullPointerException e){
            StatusBar.setStatusBar("File reading canceled");
        }
    }

    /**
     * fileChooserSaveFile method. This method opens the fileChooser and lets the user save their patient register as csv
     * If the file already exists an alert will come up from the fileChooser
     */
    private void fileChooserSaveFile(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("All Files", "*.*"));

        //Gets path where the file is to be saved
        File destination = fileChooser.showSaveDialog(App.getStage());

        if(destination != null){
            try{
                StatusBar.setStatusBar("File successfully exported as csv");
                //Saves the file
                FileManager.saveToCSV(destination.toPath().toString(), App.getPatientRegister());
            } catch (IOException e) {
                StatusBar.setStatusBar("Error: File not found");
                e.printStackTrace();
            }
        }else{
            StatusBar.setStatusBar("Error: Save destination is null");
        }
    }

}
