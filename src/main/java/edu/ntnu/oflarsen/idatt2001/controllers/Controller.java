package edu.ntnu.oflarsen.idatt2001.controllers;

/**
 * Abstract class that the different Controllers extends
 * This is to make it easier to add new Views to the project, because some methods returns the controller used in the scene
 * This gives the system higher cohesion
 */
public abstract class Controller {

}
