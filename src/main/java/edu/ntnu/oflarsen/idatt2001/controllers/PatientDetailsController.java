package edu.ntnu.oflarsen.idatt2001.controllers;

import edu.ntnu.oflarsen.idatt2001.App;
import edu.ntnu.oflarsen.idatt2001.data.Patient;
import edu.ntnu.oflarsen.idatt2001.controllers.components.StatusBar;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

/**
 * Controller for PatientDetails fxml
 * Extends the class Controller
 * This Controller is the controller for the view of the Dialog box that comes up when editing/adding patients.
 */
public class PatientDetailsController extends Controller {
    @FXML
    private TextField txtFirstName;
    @FXML
    private TextField txtLastName;
    @FXML
    private TextField txtSSN;
    @FXML
    private TextField txtGP;
    @FXML
    private TextArea txtDiagnosis;
    private String oldSSN;
    private boolean editTask = false;

    /**
     * Initializes the view. Sets the SSN TextField editable. This is to assert that if the client wants to add new patient
     * they are allowed to write in this TextField. They are not allowed to edit the SSN, therefore this is later disabled if
     * editPatient is called.
     */
    @FXML
    private void initialize(){
        txtSSN.setEditable(true);
    }

    /**
     * KeyListeners for editing/adding patients
     * ENTER = SaveAndExit
     * ESCAPE = cancel
     * @param keyEvent
     */
    @FXML
    public void keyListener(KeyEvent keyEvent){
        switch (keyEvent.getCode()){
            case ENTER:
                saveAndExit();
                break;
            case ESCAPE:
                cancel();
                break;
        }
    }

    /**
     * Method to saveAndExit changes made to the patient. Checks an boolean if the patient is being added or edited
     * The statusBar is set according to if the adding/editing was successful.
     * If there is an thrown an exception from creating/editing the patient, an alert box is being showed with the
     * exception message.
     */
    @FXML
    private void saveAndExit()
    {
        try
        {
            if(editTask){
                try {
                    App.getPatientRegister().editPatient(txtFirstName.getText(), txtLastName.getText(), txtDiagnosis.getText(), txtGP.getText(), txtSSN.getText().trim());
                    StatusBar.setStatusBar("Patient successfully edited");
                }catch (IllegalArgumentException e){
                    StatusBar.setStatusBar("Patient edit failed");
                    throw e;
                }
            }else {
                Patient patient = App.getPatientRegister().addPatient(txtFirstName.getText(), txtLastName.getText(), txtDiagnosis.getText(), txtGP.getText(), txtSSN.getText().trim());
                StatusBar.setStatusBar("Patient successfully added");
            }
            cancel();
            editTask = false;
        }
        catch(NumberFormatException nfe){
            alertErrorDialog("SSN can't be empty\nand must only contain numbers!");
        }
        catch (IllegalArgumentException e)
        {
            alertErrorDialog(e.getMessage());

        }
    }

    /**
     * Method to cancel editing/adding new patient. Closes the PatientDetails dialog
     */
    @FXML
    private void cancel()
    {
        App.closeDialog();
    }

    /**
     * Method to fill the different TextFields with the selected patient info
     * This method is for editing an existing patient
     * @param patient
     */
    public void fillPatient(Patient patient){

        txtFirstName.setText(patient.getFirstName());
        txtLastName.setText(patient.getLastName());
        txtGP.setText(patient.getGeneralPractitioner());
        txtDiagnosis.setText(patient.getDiagnosis());
        oldSSN = patient.getSSN();
        txtSSN.setText(patient.getSSN());
        txtSSN.setEditable(false);
        editTask = true;
    }

    /**
     * Private method to give the user an error alert box with the given message
     * @param message
     */
    private void alertErrorDialog(String message){
        Alert alert = new Alert(Alert.AlertType.ERROR, "Error: " + message, ButtonType.OK);
        alert.showAndWait();
    }

}
