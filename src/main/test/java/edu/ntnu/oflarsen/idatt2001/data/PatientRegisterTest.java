package edu.ntnu.oflarsen.idatt2001.data;

import edu.ntnu.oflarsen.idatt2001.data.Patient;
import edu.ntnu.oflarsen.idatt2001.data.PatientRegister;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class PatientRegisterTest {
    PatientRegister patientRegister = new PatientRegister();

    @Test
    @DisplayName("Test where test successfully adds new patient")
    public void addPatientSuccess() {
        Patient patient = patientRegister.addPatient("Olav", "Larsen","","", "12345678909");
        assertEquals(patientRegister.getPatients().stream().filter(p -> p.getSSN().equals(patient.getSSN())).findFirst().get(), patient);
    }

    @Test
    @DisplayName("Test where patient is already added to register")
    public void addPatientAlreadyAdded(){
        patientRegister.addPatient("Olav", "Larsen","","", "12345678909");
        assertThrows(IllegalArgumentException.class, ()-> patientRegister.addPatient("Olav", "Larsen","","", "12345678909"));
        try{
            patientRegister.addPatient("Olav", "Larsen","","", "12345678909");
        }catch (IllegalArgumentException e){
            assertEquals(e.getMessage(),"Patient already exists in the register");
        }
    }

    @Test
    @DisplayName("Test where the test successfully removes patient")
    public void removePatientSuccess(){
        patientRegister.addPatient("Olav", "Larsen","","", "12345678909");
        assertEquals(patientRegister.getPatients().size(), 1);
        patientRegister.removePatient("12345678909");
        assertEquals(patientRegister.getPatients().size(), 0);
    }

    @Test
    @DisplayName("Test where the patient to remove is not found")
    public void removePatientNotFound(){
        assertThrows(IllegalArgumentException.class, ()->patientRegister.removePatient("12345678909"));
    }


    @Test
    @DisplayName("Test where test successfully edits patient")
    public void editPatientTrue() {
        Patient patient = patientRegister.addPatient("Olav", "Larsen","","", "12345678909");
        try{
            patientRegister.editPatient("Lars", "Lars", "", "", "12345678909");
            assertTrue(true);
        }catch (IllegalArgumentException e){
            fail();
        }
        assertEquals(patient.getFirstName(), "Lars");
    }

    @Test
    @DisplayName("Test where test fails to edit patient")
    public void editPatientFalse() {
        Patient patient = patientRegister.addPatient("Olav", "Larsen","","", "12345678909");
        try{
            patientRegister.editPatient("Lars", "Lars", "", "", "12345678902");
            fail();
        }catch (IllegalArgumentException e){
            assertTrue(true);
        }
    }

    @Test
    @DisplayName("Test where register takes in new list of patients")
    public void newListOfPatients() {
        ArrayList<Patient> patients = patientRegister.getPatients();
        Patient patient = new Patient("Olav", "Larsen","","", "12345678909");
        PatientRegister patientRegister1 = new PatientRegister();
        patientRegister1.addPatient("Olav", "Larsen","","", "12345678909");
        patientRegister.newListOfPatients(patientRegister1);
        assertNotEquals(patients, patientRegister.getPatients());
        assertEquals(patientRegister1.getPatients(), patientRegister.getPatients());
    }

}