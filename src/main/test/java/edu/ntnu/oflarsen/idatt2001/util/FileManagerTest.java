package edu.ntnu.oflarsen.idatt2001.util;


import edu.ntnu.oflarsen.idatt2001.data.Patient;
import edu.ntnu.oflarsen.idatt2001.data.PatientRegister;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class FileManagerTest {

    @Nested
    @DisplayName("Tests for reading a csvFile")
    public class readCSV{

        @Test
        @DisplayName("Test where CSV is read successfully")
        public void readCSVSuccess(){
            try {
                //Test file with two patients
                PatientRegister patientRegister = FileManager.readCSV("src/main/resources/edu/ntnu/oflarsen/idatt2001/filesForTests/testFile.csv");
                assertEquals(patientRegister.getPatients().size(), 2);
            }catch (IOException ignored){

            }
        }

        @Test
        @DisplayName("Test where file is not found")
        public void readCSVFileNotFound(){
            try{
                assertThrows(IOException.class, ()->{
                    PatientRegister patientRegister = FileManager.readCSV("src/main/test/filesForTests/testFil");
                });
                PatientRegister patientRegister = FileManager.readCSV("src/main/test/filesForTests/testFil");
            }catch (IOException e){
                assertEquals(e.getMessage(), "File not found");
            }
        }

        @Test
        @DisplayName("Test where file is not csv")
        public void readCSVFileNotCsv(){
            try{
                assertThrows(IllegalArgumentException.class, ()->{
                    PatientRegister patientRegister = FileManager.readCSV("src/main/resources/edu/ntnu/oflarsen/idatt2001/filesForTests/notcsv.txt");
                });
                PatientRegister patientRegister = FileManager.readCSV("src/main/resources/edu/ntnu/oflarsen/idatt2001/filesForTests/notcsv.txt");
            }catch (IllegalArgumentException | IOException e){
                assertEquals(e.getMessage(), "The chosen file type is not valid");
            }
        }
    }

    PatientRegister patientRegister = new PatientRegister();
    PatientRegister patientRegisterReadFromFile = new PatientRegister();
    Patient patient1 = patientRegister.addPatient("Olav", "Larsen", "Sick ", "Lars", "12345676543");
    Patient patient2 = patientRegister.addPatient("Olav", "Lar", "Sick ", "Lars", "12345676542");
    @Nested
    @DisplayName("Tests for saving to csv. Both with and without .csv ending on paths")
    public class saveToCSV{
        @Test
        @DisplayName("Test to save csv with .csv at the end of file name")
        public void saveToCSVFileNameWithCsvEnd() {

            try {

                FileManager.saveToCSV("src/main/resources/edu/ntnu/oflarsen/idatt2001/filesForTests/fileSavedTestWithCsvEnd.csv", patientRegister);
                patientRegisterReadFromFile = FileManager.readCSV("src/main/resources/edu/ntnu/oflarsen/idatt2001/filesForTests/fileSavedTestWithCsvEnd.csv");
                assertTrue(patientRegisterReadFromFile.getPatients().contains(patient1));
                assertTrue(patientRegisterReadFromFile.getPatients().contains(patient2));
            }catch (IOException e){
                fail();
            }
        }

        @Test
        @DisplayName("Test to save file when .csv is not at the end")
        public void saveToCSVFileNameWithoutCsvEnd() {
            try {

                FileManager.saveToCSV("src/main/resources/edu/ntnu/oflarsen/idatt2001/filesForTests/fileSavedTestWithCsvEnd", patientRegister);
                patientRegisterReadFromFile = FileManager.readCSV("src/main/resources/edu/ntnu/oflarsen/idatt2001/filesForTests/fileSavedTestWithCsvEnd.csv");
                assertTrue(patientRegisterReadFromFile.getPatients().contains(patient1));
                assertTrue(patientRegisterReadFromFile.getPatients().contains(patient2));
            }catch (IOException e){
                fail();
            }
        }

    }
}