package edu.ntnu.oflarsen.idatt2001.data;

import edu.ntnu.oflarsen.idatt2001.data.Patient;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PatientTest {
    Patient testPatient = new Patient("Olav", "Larsen", "Sick", "Dr.Dre", "12345678900");
    @Nested
    @DisplayName("Tests for patient constructor")
    public class initPatient{

        @Nested
        @DisplayName("Tests for exceptions getting thrown in the constructor")
        public class createPatientThrows {

            @Test
            @DisplayName("Test where empty first name throws exception")
            public void createPatientThrowFirstname() {
                assertThrows(IllegalArgumentException.class, () -> {
                    Patient patient = new Patient("", "Larsen", "sick", "Dr.", "12345678911");
                });
                try {
                    Patient patient = new Patient("", "Larsen", "sick", "Dr.", "12345678911");
                } catch (IllegalArgumentException e) {
                    assertEquals(e.getMessage(), "First name can't be an empty string");
                }
            }

            @Test
            @DisplayName("Test where first name equals null throws exception")
            public void createPatientThrowFirstnameNull() {
                assertThrows(IllegalArgumentException.class, () -> {
                    Patient patient = new Patient(null, "Larsen", "sick", "Dr.", "12345678911");
                });
                try {
                    Patient patient = new Patient(null, "Larsen", "sick", "Dr.", "12345678911");
                } catch (IllegalArgumentException e) {
                    assertEquals(e.getMessage(), "First name can't be null");
                }
            }

            @Test
            @DisplayName("Test where empty patient last name throws exception")
            public void createPatientThrowLastname() {
                assertThrows(IllegalArgumentException.class, () -> {
                    Patient patient = new Patient("d", "", "sick", "Dr.", "12345678911");
                });
                try {
                    Patient patient = new Patient("d", "", "sick", "Dr.", "12345678911");
                } catch (IllegalArgumentException e) {
                    assertEquals(e.getMessage(), "Last name can't be an empty string");
                }
            }
            @Test
            @DisplayName("Test where patient last name is null throws exception")
            public void createPatientThrowLastnameNull() {
                assertThrows(IllegalArgumentException.class, () -> {
                    Patient patient = new Patient("d", null, "sick", "Dr.", "12345678911");
                });
                try {
                    Patient patient = new Patient("ad", null, "sick", "Dr.", "12345678911");
                } catch (IllegalArgumentException e) {
                    assertEquals(e.getMessage(), "Last name can't be null");
                }
            }
            @Test
            @DisplayName("Test where SSN too big throws an exception")
            public void createPatientThrowsSSNTooBig(){
                assertThrows(IllegalArgumentException.class, () -> {
                    Patient patient = new Patient("d", "j", "sick", "Dr.Dre", "123456789111");
                });
                try {
                    Patient patient = new Patient("d", "j", "sick", "Dr.Dre", "123456789111");
                } catch (IllegalArgumentException e) {
                    assertEquals(e.getMessage(), "SNN must be 11 digits");
                }
            }
            @Test
            @DisplayName("Test where SSN too small throws an exception")
            public void createPatientThrowsSSNTooSmall(){
                assertThrows(IllegalArgumentException.class, () -> {
                    Patient patient = new Patient("d", "d", "sick", "Dr.Dre", "123456789");
                });
                try {
                    Patient patient = new Patient("d", "d", "sick", "Dr.Dre", "123456789");
                } catch (IllegalArgumentException e) {
                    assertEquals(e.getMessage(), "SNN must be 11 digits");
                }
            }
        }
    }
    @Nested
    @DisplayName("Tests for setFirstName")
    public class setFirstName{

        @Test
        @DisplayName("Test where first name is successfully set")
        public void setFirstNameSuccess() {
            testPatient.setFirstName("Olav F. P.");
        }

        @Test
        @DisplayName("Test where first name empty string throws")
        public void setFirstNameEmptyStringThrows(){
            assertThrows(IllegalArgumentException.class, ()-> testPatient.setFirstName(""));
            try {
                testPatient.setFirstName("");
            }catch (IllegalArgumentException e){
                assertEquals(e.getMessage(), "First name can't be an empty string");
            }
        }

        @Test
        @DisplayName("Test where set name is null throws")
        public void setFirstNameNullStringThrows(){
            assertThrows(IllegalArgumentException.class, ()-> testPatient.setFirstName(null));
            try {
                testPatient.setFirstName(null);
            }catch (IllegalArgumentException e){
                assertEquals(e.getMessage(), "First name can't be null");
            }
        }
    }

    @Nested
    @DisplayName("Tests for setLastName")
    public class setLastName{

        @Test
        @DisplayName("Test where set last name succeeds")
        public void setLastNameSuccess() {
            testPatient.setLastName("Larsen");
        }

        @Test
        @DisplayName("Test where empty last name throws")
        public void setLastNameEmptyStringThrows(){
            assertThrows(IllegalArgumentException.class, ()-> testPatient.setLastName(""));
            try {
                testPatient.setLastName("");
            }catch (IllegalArgumentException e){
                assertEquals(e.getMessage(), "Last name can't be an empty string");
            }
        }

        @Test
        @DisplayName("Test where last name set null throws")
        public void setLastNameNullStringThrows(){
            assertThrows(IllegalArgumentException.class, ()-> testPatient.setLastName(null));
            try {
                testPatient.setLastName(null);
            }catch (IllegalArgumentException e){
                assertEquals(e.getMessage(), "Last name can't be null");
            }
        }
    }
    @Nested
    @DisplayName("Tests for equals method")
    public class testEquals{
        Patient testPatient2 = new Patient("Olav F. P.", "Larsen", "", "", "12323232323");
        @Test
        @DisplayName("Test where patients are equal")
        public void testEqualsEquals() {
            assertEquals(testPatient, testPatient);
        }
        @Test
        @DisplayName("Test where patients are not equal")
        public void testEqualsNotEquals() {
            assertNotEquals(testPatient2, testPatient);
        }
    }
}